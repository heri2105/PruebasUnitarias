/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PruebasUnitarias;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Heriberto
 */
public class CalcularPersonasTest {

    public CalcularPersonasTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of calcularPersonasTiempoParaArreglos method, of class
     * CalcularPersonas.
     */
    @Test
    public void testCalcularPersonasTiempoParaArreglos() {
        System.out.println("Calcular personas por cantidad de arreglos 15");
        int n = 15;
        CalcularPersonas instance = new CalcularPersonas();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        System.out.println("numero de Personas " + result[0]);
        System.out.println("Tiempo en minutos  " + result[1]);
    }

    @Test
    public void testCalcularPersonasTiempoParaArreglosNegativo() {
        System.out.println("test para arreglos negativo -2");
        int n = -2;
        CalcularPersonas instance = new CalcularPersonas();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        System.out.println("numero de Personas " + result[0]);
        System.out.println("Tiempo en minutos  " + result[1]);

    }

    @Test
    public void testCalcularPersonasTiempoParaArreglosCero() {
        System.out.println("test para arreglos con cantidad 0");
        int n = 0;
        CalcularPersonas instance = new CalcularPersonas();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        System.out.println("numero de Personas " + result[0]);
        System.out.println("Tiempo en minutos  " + result[1]);
    }

    @Test
    public void testCalcularPersonasTiempoParaArreglos20a49() {
        System.out.println("test para arreglos entre  un rango de 20 a 49, arreglos : 49");
        int n = 49;
        CalcularPersonas instance = new CalcularPersonas();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        System.out.println("numero de Personas " + result[0]);
        System.out.println("Tiempo en minutos  " + result[1]);
    }

    @Test
    public void testCalcularPersonasTiempoParaArreglosmayora50() {
        System.out.println("test para arreglos mayor a 50, arreglos 75");
        int n = 75;
        CalcularPersonas instance = new CalcularPersonas();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        System.out.println("numero de Personas " + result[0]);
        System.out.println("Tiempo en minutos  " + result[1]);

    }

    @Test
    public void testCalcularPersonasTiempoParaArreglosConvirtiendoDouble() {
        System.out.println("test para arreglos convirtiendo un numero double en int,arreglo 1.2");
        int n = 75;
        CalcularPersonas instance = new CalcularPersonas();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos((int) 1.2);
        System.out.println("numero de Personas " + result[0]);
        System.out.println("Tiempo en minutos  " + result[1]);

    }

}
