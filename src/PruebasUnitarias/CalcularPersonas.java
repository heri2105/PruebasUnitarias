package PruebasUnitarias;
//al regresar el array de entero regresaremos en la primera posicion 
//el numero de personas para esa cantidad de arreglos y en las segunda posicion 
//regresara el timepo que se necesita para completar la tarea en minutos

public class CalcularPersonas {

    public int[] calcularPersonasTiempoParaArreglos(int n) {

        int tiempoPorArreglo = 15;
        int[] resultado = new int[2];

        if (n >= 1 && n <= 19) {
            resultado[0] = 1;
            resultado[1] = n * tiempoPorArreglo;
        }
        if (n >= 20 && n <= 49) {
            resultado[0] = 2;
            resultado[1] = n * tiempoPorArreglo;
        }
        if (n >= 50) {
            int personas = (n / 25);
            int personaExtra = 1;
            resultado[0] = personas + personaExtra;
            resultado[1] = n * tiempoPorArreglo;
        }

        return resultado;
    }

}
